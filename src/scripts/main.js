document.addEventListener("DOMContentLoaded", () => {

    const headerBurgerBtn = document.querySelector(".js-header__burger");
    const headerMenu = document.querySelector(".js-header__menu");
    const headerExitBtn = document.querySelector(".js-header__exit");
    const headerMenuItem = document.querySelectorAll(".js-header__nav-item");

    function checkButton (event) {
        let targetButton = event.target.closest("button");

        if (targetButton === headerBurgerBtn) {
            headerMenu.classList.toggle("visible-menu");
            headerBurgerBtn.classList.toggle("visible-button");
            headerExitBtn.classList.toggle("visible-button");
        } else if (targetButton === headerExitBtn) {
            headerMenu.classList.toggle("visible-menu");
            headerBurgerBtn.classList.toggle("visible-button");
            headerExitBtn.classList.toggle("visible-button");
        } else {
            headerMenu.classList.remove("visible-menu");
            headerBurgerBtn.classList.add("visible-button");
            headerExitBtn.classList.remove("visible-button");
        }
    };

    function checkMenuItem (event) {
        let targetMenuItem = event.target.closest("li");
        
        if (targetMenuItem !== null) {

            headerMenuItem.forEach(elem => {
                elem.classList.remove("active");
    
                if (targetMenuItem === elem) {
                    elem.classList.add("active");
                };
            });
        };
    };

    document.addEventListener("click", event => {

        checkButton(event);
        checkMenuItem(event);
    });

})